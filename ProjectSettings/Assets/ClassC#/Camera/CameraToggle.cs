/** program in C# by Trochon Loïc
Objective: Use toggleGroup to choise mode of camera moving.
Details:  Key are define in Unity ("Edit" -> "Projet Setting" -> "Input Manager" -> "Axes")
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraToggle : MonoBehaviour
{

    #region Variable
    public Toggle toggle1;
    public Toggle toggle2;
    public Toggle toggle3;
    public Toggle toggle4;
    public Toggle toggle5;
    public Toggle toggle6;

    private Camera cam;
    
    private Vector3 pointZeroTranslate = new Vector3();
    private Quaternion pointZeroRotate = new Quaternion();

    #endregion

    #region main
    void Start()
    {
        cam = GetComponent<Camera> ();
        

        pointZeroTranslate = transform.position;
        pointZeroRotate = transform.rotation;
    }

    void Update()
    {
        if(toggle1 != null) // résolution de l'error: NullReferenceException
        {
            switchMoveMode();
        } 
               
        
        zoom();       
            
        retourAuPointZero();
    }
    #endregion

    #region Methode
    public void switchMoveMode() //Key: Up / Down Arrow
    {
        if(toggle1.isOn)
        {
            translationZ();
        } 
        else if(toggle2.isOn)
        {
            translationY();
        }
        else if(toggle3.isOn)
        {
            translationX();
        }
        else if(toggle4.isOn)
        {
            rotationZ();
        }
        else if(toggle5.isOn)
        {
            rotationY();
        }
        else if(toggle6.isOn)
        {
            rotationX();
        }
    }

    public void translationZ()
    {
        transform.Translate (
            0,
            0,
            Input.GetAxis("Deplacement") * Time.deltaTime * 5f 
            );
    }
    public void translationY()
    {
        transform.Translate (
            0,
            Input.GetAxis("Deplacement") * Time.deltaTime * 5f,
            0 
            );
    }
    public void translationX()
    {
        transform.Translate (
            Input.GetAxis("Deplacement") * Time.deltaTime * 5f,
            0,
            0 
            );
    }
    public void rotationZ()
    {
        transform.Rotate(
            0, 
            0, 
            Input.GetAxis("Deplacement")  
            );
    }
    public void rotationY()
    {
        transform.Rotate(
            0, 
            Input.GetAxis("Deplacement"), 
            0 
            );
    }
    public void rotationX()
    {
        transform.Rotate(
            Input.GetAxis("Deplacement"), 
            0, 
            0  
            );
    }

    public void zoom()
    {
        
        if(Input.GetAxis("Zoom")>0) //Key: v / n
        {
            cam.fieldOfView-= 0.2f;
        }
        if((Input.GetAxis("Zoom")<0) && (GetComponent<Camera> ().fieldOfView<70))
        {
            cam.fieldOfView+= 0.2f;
        }
    }

    public void retourAuPointZero()
    {
        if(Input.GetAxis("Reset")>0) //Key: space
        {
            transform.position = pointZeroTranslate;
            transform.rotation = pointZeroRotate;
        }
    }
    #endregion
}
