/** program in C# by Trochon Loïc
Objective: move the camera with the keyboard keys
Details:  Key are define in Unity ("Edit" -> "Projet Setting" -> "Input Manager" -> "Axes")
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraKeyboard : MonoBehaviour
{
    #region Variables
    private Camera cam;
    

    private Vector3 pointZeroTranslate = new Vector3();
    private Quaternion pointZeroRotate = new Quaternion();

    #endregion

    #region main
        void Start()
        {
            cam = GetComponent<Camera> ();
            pointZeroTranslate = transform.position;
            pointZeroRotate = transform.rotation;
        }

        void Update()
        {
           
            translation();

            rotation();

            zoom();       
            
            retourAuPointZero();
        }
    #endregion

    #region Methode
    public void translation()
    {
        transform.Translate (
            Input.GetAxis("AxeX_translate") * Time.deltaTime * 5f , //key: q / d
            Input.GetAxis("AxeY_translate") * Time.deltaTime * 5f, //key: a / e
            Input.GetAxis("AxeZ_translate") * Time.deltaTime * 5f //key: s / z
            );
    }

    public void rotation()
    {
        transform.Rotate(
            Input.GetAxis("AxeX_rotate"), //Key: o / l
            Input.GetAxis("AxeY_rotate"), //Key: k / m
            Input.GetAxis("AxeZ_rotate")  //KKey: i / p
            );
    }

    public void zoom()
    {
        
        if(Input.GetAxis("Zoom")>0) //Key: v / n
        {
            cam.fieldOfView-= 0.2f;
        }
        if((Input.GetAxis("Zoom")<0) && (GetComponent<Camera> ().fieldOfView<70))
        {
            cam.fieldOfView+= 0.2f;
        }
    }

    public void retourAuPointZero()
    {
        if(Input.GetAxis("Reset")>0) //Key: space
        {
            transform.position = pointZeroTranslate;
            transform.rotation = pointZeroRotate;
        }
    }
    #endregion
}
