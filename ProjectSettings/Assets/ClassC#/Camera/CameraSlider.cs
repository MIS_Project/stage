/** program in C# by Trochon Loïc
Objective: move the camera with sliders
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSlider : MonoBehaviour
{
    #region Variable
    private Camera cam;    
    public Slider sliderTX ;
    public Slider sliderTY ;
    public Slider sliderTZ ;
    public Slider sliderRX ;
    public Slider sliderRY ;
    public Slider sliderRZ ;
    public Slider sliderZoom ;


    private Vector3 pointZeroTranslate = new Vector3();
    private Quaternion pointZeroRotate = new Quaternion();
    private float pointZeroZoom;  
    #endregion


    #region main
    void Start()
    {
        cam = GetComponent<Camera>();
        pointZeroTranslate = transform.position;
        pointZeroRotate = transform.rotation;
        pointZeroZoom = cam.fieldOfView;
    }

    void Update()
    {
        
    }
    #endregion

    #region Methode

    public void TranslationX(float nb){
        transform.position =  new Vector3(nb, transform.position.y , transform.position.z);
    } 
    public void TranslationY(float nb){
        transform.position =  new Vector3(transform.position.x, nb, transform.position.z);
    } 
    public void TranslationZ(float nb){
        transform.position = new Vector3(transform.position.x, transform.position.y, nb);
    } 

    public void RotationX(float nb){
        transform.rotation= new Quaternion(nb, transform.rotation.y, transform.rotation.z, transform.rotation.w );
    }
    public void RotationY(float nb){
        transform.rotation= new Quaternion(transform.rotation.x, nb, transform.rotation.z, transform.rotation.w );
    }
    public void RotationZ(float nb){
        transform.rotation= new Quaternion(transform.rotation.x, transform.rotation.y, nb, transform.rotation.w );
    }
    public void SliderZoom(float nb){
        cam.fieldOfView= nb;
    }
    
    public void reset()
    { 
        if (sliderTX !=null)
            sliderTX.value = 0;

        if (sliderTY !=null)
            sliderTY.value = 0;

        if (sliderTZ !=null)
            sliderTZ.value = 0;
        
        if (sliderRX !=null)
            sliderRX.value = 0;

        if (sliderRY !=null)
            sliderRY.value = 0;

        if (sliderRZ !=null)
            sliderRZ.value = 0;

        if (sliderZoom != null)
            sliderZoom.value = 60;

        transform.position = pointZeroTranslate;
        transform.rotation = pointZeroRotate;
        cam.fieldOfView = pointZeroZoom;
    }
    
    #endregion
}
