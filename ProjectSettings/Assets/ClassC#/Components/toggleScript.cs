/** program in C# by Trochon Loïc
Objective: enable or disable the panel under the toggle 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class toggleScript : MonoBehaviour
{
    public GameObject menu;
    
    public void openPanel(bool value)
    {
        menu.gameObject.SetActive(value);
    }
}
