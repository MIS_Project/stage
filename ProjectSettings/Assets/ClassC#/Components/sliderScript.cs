/** program in C# by Trochon Loïc
Objective: link the slider intensity of My Blur with it Text to display it value 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class sliderScript : MonoBehaviour
{
    private Text textSlider;
    void Start()
    {
        textSlider = GetComponent<Text> ();
    }

    public void printValue(float nb){
        textSlider.text = Mathf.RoundToInt(100 - (100 * nb/10)) + "%";
    }
}
