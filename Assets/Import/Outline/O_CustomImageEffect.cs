﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class O_CustomImageEffect : MonoBehaviour
{
    public Material imageEffect;
    public FlexibleColorPicker outlineColor;
    private float outlineWidth = 1f;
        

    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        imageEffect.SetFloat("_OutlineWidth", outlineWidth);
        imageEffect.SetColor("_OutlineColor", outlineColor.color);
    
        if (imageEffect != null)
            Graphics.Blit(src, dest, imageEffect);
        else
            dest = src;
    }

    
    public void valueOutlineWidth(float nb)  // permet de changer l'épaisseur des contours via un slider
        {
            outlineWidth = nb;           
        }
}
