/** program in C# by Trochon Loïc
Objective: écrit la valeur d'un slider en pourcentage dans le texte donné en paramètre
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextSlider : MonoBehaviour
{
    private Text textSlider;
    void Start()
    {
        textSlider = GetComponent<Text> ();
    }

    public void printValueTextBlur(float nb){
        textSlider.text = Mathf.RoundToInt(100 - (100 * nb/10)) + "%";
    }

    public void printValueOutlineWidth(float nb){
        textSlider.text = Mathf.RoundToInt(100 * nb/5) + "%";
    }
}
