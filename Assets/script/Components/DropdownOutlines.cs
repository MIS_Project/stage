/** program in C# by Trochon Loïc
Objective: faire une liste de shader sur un Dropdown. 
Detail: Change le matériel du script O_CustomImageEffect qui s'applique sur la caméra selon la valeur choisi dans la liste
Pour ajouter un shader:
//1// Ajouter un "public Material shaderName" 
//2// Faire un "items.Add("shader name")" pour ajouter un objet a la liste
//3// Ajouter un "else if (index==n){myMaterial= shaderName} }
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropdownOutlines : MonoBehaviour
{
    public O_CustomImageEffect customImage;
    public Material allInOne;
    public Material linear01based;
    public Material depthBased;
    public Material textureBased;
    //1//
    private Material myMaterial;

    // Start is called before the first frame update
    void Start()
    {
        var dropdown = transform.GetComponent<Dropdown> ();

        dropdown.options.Clear();

        List<string> items = new List<string>();
        items.Add("All-in-One");
        items.Add("Linear 01 Depth");
        items.Add("Depth Based");
        items.Add("Texture Based");
        //2//

        foreach(var item in items)
        {
            dropdown.options.Add(new Dropdown.OptionData() { text = item });
        }

        DropdownItemSelected(dropdown);
        dropdown.onValueChanged.AddListener(delegate {DropdownItemSelected(dropdown); });
    }

    void DropdownItemSelected(Dropdown dropdown)
    {
        int index = dropdown.value;

        if(index==0)
        {
            myMaterial = allInOne;
        }
        else if(index==1)
        {
            myMaterial = linear01based;
        }
        else if(index==2)
        {
            myMaterial = depthBased;
        }
        else if(index==3)
        {
            myMaterial = textureBased;
        }
        //3//
        customImage.imageEffect = myMaterial;
        
    }

    
}
